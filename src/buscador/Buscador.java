/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscador;

/**
 *Clase que tiene metodos para buscar de forma
 * secuencial y binaria sobre arreglos del tipo E
 * @author Irving Alexandro Diaz Tapia
 * @version 1.0
 * @param <E>
 */
public class Buscador<E extends Comparable<E>>{
    E elementos[];

    /**
     * Constructor del buscador por parametros
     * que recibe un arreglo de elementos del tipo E
     * @param elementos 
     */
    public Buscador(E[] elementos) {
        this.elementos = elementos;
    }
    /**
     * Metodo que hace una busqueda secuencial y recible 
     * el elemento a buscar
     * Regresa 1 si encontro al elemento o -1 si no lo encontro
     * @param elemento
     * @return 
     */
    public int hacerBusquedaSecuencial(E elemento){
        int e=-1;
        for(int i = 0; i < elementos.length; i++) {
            if(elementos[i].equals(elemento)){
                e = i-1;
                break;
            }
        }
        return e;
    }
    /**
     * Metodo de busqueda binaria que recibe un elemento a buscar en el arreglo
     * Regresa -1 si nolo encontro o 1 si lo encuentra
     * @param elemento
     * @return -1 o 1
     */
    public int hacerBusquedaBinaria(E elemento){
        int n = elementos.length-1;
        int m = 0;
        int i;
        while(m<=n){
            i =(m+n)/2;
            if(elementos[i].equals(elemento))
                return i-1;
            else if(elementos[i].compareTo(elemento)==-1)
                m = i+1;
            else
                n = i-1;
        }
        return -1;
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
    }
    
}
